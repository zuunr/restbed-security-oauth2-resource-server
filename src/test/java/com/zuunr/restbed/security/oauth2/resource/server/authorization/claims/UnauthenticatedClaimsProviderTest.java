/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authorization.claims;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.authorization.claims.ClaimsNamespaceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfig;

import reactor.core.publisher.Mono;

class UnauthenticatedClaimsProviderTest {
    
    @Test
    void givenDefaultClaimsDisabledShouldReturnEmptyClaims() {
        UnauthenticatedClaimsProvider unauthenticatedClaimsProvider = new UnauthenticatedClaimsProvider(new ClaimsNamespaceConfig(null, null), apiName -> {
           return new ServiceConfig(JsonObject.EMPTY); 
        });
        
        Mono<JsonObject> unauthenticatedClaims = unauthenticatedClaimsProvider.getUnauthenticatedClaims("myApi");
        
        assertEquals(JsonObject.EMPTY, unauthenticatedClaims.block());
    }
    
    @Test
    void givenDefaultClaimsManuallyDisabledShouldReturnAnonymousClaims() {
        UnauthenticatedClaimsProvider unauthenticatedClaimsProvider = new UnauthenticatedClaimsProvider(new ClaimsNamespaceConfig(null, null), apiName -> {
            return new ServiceConfig(JsonObject.EMPTY
                    .put(JsonArray.ofDotSeparated("config.security.unauthenticated.enableDefaultClaims"), false)); 
         });
         
         Mono<JsonObject> unauthenticatedClaims = unauthenticatedClaimsProvider.getUnauthenticatedClaims("myApi");
         
         assertEquals(JsonObject.EMPTY, unauthenticatedClaims.block());
    }
    
    @Test
    void givenDefaultClaimsEnabledShouldReturnAnonymousClaims() {
        UnauthenticatedClaimsProvider unauthenticatedClaimsProvider = new UnauthenticatedClaimsProvider(new ClaimsNamespaceConfig("ctx", "roles"), apiName -> {
            return new ServiceConfig(JsonObject.EMPTY
                    .put(JsonArray.ofDotSeparated("config.security.unauthenticated.enableDefaultClaims"), true)); 
         });
         
         Mono<JsonObject> unauthenticatedClaims = unauthenticatedClaimsProvider.getUnauthenticatedClaims("myApi");
         
         JsonObject expectedClaims = JsonObject.EMPTY
                 .put("ctx", "anonymous")
                 .put("roles", JsonArray.EMPTY
                         .add("ANONYMOUS"));
         
         assertEquals(expectedClaims, unauthenticatedClaims.block());
    }
}
