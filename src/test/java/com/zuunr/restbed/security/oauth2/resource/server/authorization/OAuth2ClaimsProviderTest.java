/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authorization;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.security.oauth2.resource.server.authorization.claims.UnauthenticatedClaimsProvider;

import reactor.core.publisher.Mono;

class OAuth2ClaimsProviderTest {
    
    @Test
    void givenTokenShouldExtractClaims() {
        String bearerToken = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiYXVkIjpbImh0dHBzOi8vcmVzdGJlZC56dXVuci5jb20iXSwiaHR0cHM6Ly9yZXN0YmVkLnp1dW5yLmNvbS9lbWFpbCI6ImxhdXJhLmFuZGVyc3NvbkBleGFtcGxlLmNvbSIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwiLCJleHAiOjE1ODcwNjEzMDUsImlhdCI6MTUxNjIzOTAyMiwiaHR0cHM6Ly9yZXN0YmVkLnp1dW5yLmNvbS9yb2xlcyI6WyJNRU1CRVIiLCJBRE1JTiJdfQ.dm0XDwBJMTmQtQRntlKswVWN8H9cUsdcinwix9h13sK2AcMutEKR5ppEX8QjBAM24vNrZCg5lkS9zOZMKN12Bg";
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer " + bearerToken)));
        
        OAuth2ClaimsProvider oAuth2ClaimsProvider = new OAuth2ClaimsProvider(new AuthorizationBearerProvider(), new JsonObjectFactory(), new UnauthenticatedClaimsProvider(null, null));
        JsonObject claims = oAuth2ClaimsProvider.getClaims(request).block();
        
        JsonObject expectedClaims = JsonObject.EMPTY
                .put("https://restbed.zuunr.com/email", "laura.andersson@example.com")
                .put("https://restbed.zuunr.com/roles", JsonArray.EMPTY
                        .add("MEMBER")
                        .add("ADMIN"))
                .put("exp", 1587061305000L)
                .put("iat", 1516239022000L)
                .put("scope", "openid profile email")
                .put("sub", "1234567890")
                .put("aud", JsonArray.EMPTY
                        .add("https://restbed.zuunr.com"));
        
        assertEquals(expectedClaims, claims);
    }
    
    @Test
    void givenFaultyOrMissingTokenShouldReturnEmptyJsonObject() {
        String bearerToken = "faultyToken";
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer " + bearerToken)));
        
        OAuth2ClaimsProvider oAuth2ClaimsProvider = new OAuth2ClaimsProvider(new AuthorizationBearerProvider(), new JsonObjectFactory(), new UnauthenticatedClaimsProvider(null, null));
        JsonObject claims = oAuth2ClaimsProvider.getClaims(request).block();
        
        assertEquals(JsonObject.EMPTY, claims);
    }
    
    @Test
    void givenFaultyAuthorizationHeaderShouldReturnEmptyClaims() {
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bear")));
        
        OAuth2ClaimsProvider oAuth2ClaimsProvider = new OAuth2ClaimsProvider(new AuthorizationBearerProvider(), new JsonObjectFactory(), new UnauthenticatedClaimsProvider(null, null));
        JsonObject claims = oAuth2ClaimsProvider.getClaims(request).block();
        
        assertEquals(JsonObject.EMPTY, claims);
    }
    
    @Test
    void givenMissingTokenShouldReturnDefaultClaims() {        
        final JsonObject expectedClaims = JsonObject.EMPTY
                .put("ctx", "anonymous")
                .put("roles", JsonArray.EMPTY
                        .add("ANONYMOUS"));
        
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .url("https://example.com/v1/myApi/api/users")
                .headers(JsonObject.EMPTY);
        
        OAuth2ClaimsProvider oAuth2ClaimsProvider = new OAuth2ClaimsProvider(new AuthorizationBearerProvider(), new JsonObjectFactory(), new UnauthenticatedClaimsProvider(null, null) {
            @Override
            public Mono<JsonObject> getUnauthenticatedClaims(String apiName) {
                return Mono.just(expectedClaims);
            }
        });
        JsonObject claims = oAuth2ClaimsProvider.getClaims(request).block();
        
        assertEquals(expectedClaims, claims);
    }
}
