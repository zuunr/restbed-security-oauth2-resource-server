/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authentication;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.core.OAuth2TokenValidatorResult;
import org.springframework.security.oauth2.jwt.Jwt;

class Auth0AudienceValidatorTest {
    
    private Auth0AudienceValidator auth0AudienceValidator = new Auth0AudienceValidator("https://example.com");
    
    @Test
    void givenMatchingAudienceShouldReturnSuccess() {
        Jwt jwt = Jwt.withTokenValue("token")
                .audience(Lists.list("https://example.com"))
                .header("myHeader", "myValue")
                .build();
        
        OAuth2TokenValidatorResult result = auth0AudienceValidator.validate(jwt);
        
        assertFalse(result.hasErrors());
    }
    
    @Test
    void givenNonMatchingAudienceShouldReturnFailure() {
        Jwt jwt = Jwt.withTokenValue("token")
                .audience(Lists.list("https://sub.example.com"))
                .header("myHeader", "myValue")
                .build();
        
        OAuth2TokenValidatorResult result = auth0AudienceValidator.validate(jwt);
        
        assertTrue(result.hasErrors());
    }
}
