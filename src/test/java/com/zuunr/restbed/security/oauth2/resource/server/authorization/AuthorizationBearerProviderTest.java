/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authorization;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.exchange.Method;
import com.zuunr.restbed.core.exchange.Request;

class AuthorizationBearerProviderTest {
    
    private AuthorizationBearerProvider authorizationBearerProvider = new AuthorizationBearerProvider();
    
    @Test
    void givenBearerShouldReturnIt() {
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer 123456789")));
        
        String bearer = authorizationBearerProvider.getBearer(request);
        
        assertEquals("123456789", bearer);
    }
    
    @Test
    void givenNoBearerShouldReturnNull() {
        Request<JsonObjectWrapper> request = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY);
        
        String bearer = authorizationBearerProvider.getBearer(request);
        
        assertNull(bearer);
    }
    
    @Test
    void givenMalformedBearerShouldThrowException() {
        Request<JsonObjectWrapper> request1 = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearerr 123456789")));
        
        assertThrows(BearerException.class, () -> authorizationBearerProvider.getBearer(request1));
        
        Request<JsonObjectWrapper> request2 = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bear 123456789")));
        
        assertThrows(BearerException.class, () -> authorizationBearerProvider.getBearer(request2));
        
        Request<JsonObjectWrapper> request3 = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("abcdef 123456789")));
        
        assertThrows(BearerException.class, () -> authorizationBearerProvider.getBearer(request3));
        
        Request<JsonObjectWrapper> request4 = Request.<JsonObjectWrapper>create(Method.GET)
                .headers(JsonObject.EMPTY
                        .put("authorization", JsonArray.EMPTY
                                .add("Bearer123456789")));
        
        assertThrows(BearerException.class, () -> authorizationBearerProvider.getBearer(request4));
    }
}
