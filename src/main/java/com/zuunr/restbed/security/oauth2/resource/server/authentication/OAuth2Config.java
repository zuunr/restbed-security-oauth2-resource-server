/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authentication;

import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

/**
 * <p>Configuration for the OAuth2 resource server
 * module.</p>
 *
 * @author Mikael Ahlberg
 */
@ConfigurationProperties(prefix = "zuunr.security.oauth2.resourceserver")
@Validated
public class OAuth2Config {
    
    @Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}")
    private String issuer;
    
    @NotEmpty(message = "You must set the api audience, e.g. https://myapi.example.com")
    private final String apiAudience;
    
    @ConstructorBinding
    public OAuth2Config(String apiAudience) {
        this.apiAudience = apiAudience;
    }

    public String getIssuer() {
        return issuer;
    }

    public String getApiAudience() {
        return apiAudience;
    }
}
