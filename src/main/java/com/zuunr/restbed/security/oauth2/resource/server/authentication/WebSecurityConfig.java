/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtValidators;
import org.springframework.security.oauth2.jwt.NimbusReactiveJwtDecoder;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoders;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.security.web.server.util.matcher.NegatedServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatchers;
import org.springframework.stereotype.Component;

import com.zuunr.restbed.core.endpoint.HttpEndpoint;

/**
 * <p>The WebSecurityConfig is responsible for setting up Spring security
 * for the Oauth2 resource server module.</p>
 * 
 * <p>Copied from Auth0 example app.</p>
 * 
 * <p>All request paths have permitAll() meaning: regardless if the user provides a
 * token, the request is forwarded to core {@link HttpEndpoint}. However, spring security
 * is still validating the authenticity of the token and stops the request if the
 * user provides a faulty or expired token.</p>
 * 
 * <p>CSRF is disabled since we are using Authorization Bearers. CSRF filter is ignored
 * by default when enabling oauth2 resource server, however if no token is provided
 * the CSRF filter is not ignored, effectively stopping possible anonymous calls.</p>
 * 
 * <p>The endpoint /errors/** is ignored since it is used by spring error page.</p>
 * 
 * @author Mikael Ahlberg
 */
@EnableWebFluxSecurity
@Component
public class WebSecurityConfig {
    
    private OAuth2Config oAuth2Config;
    
    @Autowired
    public WebSecurityConfig(OAuth2Config oAuth2Config) {
        this.oAuth2Config = oAuth2Config;
    }
    
    @Bean
    public SecurityWebFilterChain securitygWebFilterChain(ServerHttpSecurity http) {
        return http.securityMatcher(new NegatedServerWebExchangeMatcher(pathsToIgnore()))
                .authorizeExchange()
                    .anyExchange().permitAll()
                .and()
                .csrf().disable()
                    .oauth2ResourceServer().jwt().and()
                    .and()
                .build();
    }
    
    private ServerWebExchangeMatcher pathsToIgnore() {
        return ServerWebExchangeMatchers.pathMatchers(
                "/error/**"
                );
    }
    
    /**
     * <p>By default, Spring Security does not validate the "aud" claim of the token,
     * to ensure that this token is indeed intended for our app.</p>
     * 
     * @return a configured JwtDecoder with added "aud" validation
     */
    @Bean
    ReactiveJwtDecoder jwtDecoder() {
        NimbusReactiveJwtDecoder jwtDecoder = (NimbusReactiveJwtDecoder) ReactiveJwtDecoders.fromOidcIssuerLocation(oAuth2Config.getIssuer());

        OAuth2TokenValidator<Jwt> audienceValidator = new Auth0AudienceValidator(oAuth2Config.getApiAudience());
        OAuth2TokenValidator<Jwt> withIssuer = JwtValidators.createDefaultWithIssuer(oAuth2Config.getIssuer());
        OAuth2TokenValidator<Jwt> withAudience = new DelegatingOAuth2TokenValidator<>(withIssuer, audienceValidator);

        jwtDecoder.setJwtValidator(withAudience);

        return jwtDecoder;
    }
}
