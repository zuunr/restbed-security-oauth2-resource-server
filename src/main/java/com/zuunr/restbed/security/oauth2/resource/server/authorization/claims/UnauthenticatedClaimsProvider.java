/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authorization.claims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zuunr.json.JsonArray;
import com.zuunr.json.JsonObject;
import com.zuunr.restbed.core.authorization.claims.ClaimsNamespaceConfig;
import com.zuunr.restbed.core.restservice.ServiceConfigProvider;

import reactor.core.publisher.Mono;

/**
 * <p>The UnauthenticatedClaimsProvider is responsible
 * for providing a claims object when the user is 
 * unauthenticated (meaning a token was not provided).</p>
 * 
 * <p>It is possible to enable default claims containing
 * an ANONYMOUS role. This is done through setting
 * config.security.unauthenticated.enableDefaultClaims
 * to true in service scheme config.</p>
 *
 * @author Mikael Ahlberg
 */
@Component
public class UnauthenticatedClaimsProvider {
    
    private ClaimsNamespaceConfig claimsNamespaceConfig;
    private ServiceConfigProvider serviceConfigProvider;
    
    @Autowired
    public UnauthenticatedClaimsProvider(
            ClaimsNamespaceConfig claimsNamespaceConfig,
            ServiceConfigProvider serviceConfigProvider) {
        this.claimsNamespaceConfig = claimsNamespaceConfig;
        this.serviceConfigProvider = serviceConfigProvider;
    }
    
    /**
     * <p>Returns a default claims object as a {@link JsonObject} for an
     * unauthenticated user.</p>
     * 
     * <p>If anonymous default claim is enabled, the method will return
     * a claim containing the role ANONYMOUS.</p>
     * 
     * @param apiName is used to retrieve the service config
     * @return a claims object
     */
    public Mono<JsonObject> getUnauthenticatedClaims(String apiName) {
        return Mono.defer(() -> Mono.just(apiName)
                .map(this::isUnauthenticatedDefaultClaimsEnabled)
                .map(result -> Boolean.TRUE.equals(result)
                        ? JsonObject.EMPTY
                                .put(claimsNamespaceConfig.getContextClaimNamespace(), "anonymous")
                                .put(claimsNamespaceConfig.getRolesClaimNamespace(), JsonArray.EMPTY
                                        .add("ANONYMOUS"))
                        : JsonObject.EMPTY));
    }
    
    private boolean isUnauthenticatedDefaultClaimsEnabled(String apiName) {
        return serviceConfigProvider.getServiceConfig(apiName).asJsonObject().get(JsonArray.ofDotSeparated("config.security.unauthenticated.enableDefaultClaims"), false).getValue(Boolean.class);
    }
}
