/*
 * Copyright 2020 Zuunr AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.zuunr.restbed.security.oauth2.resource.server.authorization;

import java.text.ParseException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nimbusds.jwt.JWTParser;
import com.zuunr.json.JsonObject;
import com.zuunr.json.JsonObjectFactory;
import com.zuunr.json.util.JsonObjectWrapper;
import com.zuunr.restbed.core.authorization.claims.ClaimsProvider;
import com.zuunr.restbed.core.exchange.Request;
import com.zuunr.restbed.security.oauth2.resource.server.authorization.claims.UnauthenticatedClaimsProvider;

import reactor.core.publisher.Mono;

/**
 * <p>Oauth2 resource server implementation of the {@link ClaimsProvider} interface.
 * The OAuth2ClaimsProvider extracts the claims from the provided, and _already_ authenticated,
 * jwt bearer token.</p>
 * 
 * @see ClaimsProvider
 *
 * @author Mikael Ahlberg
 */
@Component
public class OAuth2ClaimsProvider implements ClaimsProvider {
    
    private final Logger logger = LoggerFactory.getLogger(OAuth2ClaimsProvider.class);
    
    private AuthorizationBearerProvider authorizationBearerProvider;
    private JsonObjectFactory jsonObjectFactory;
    private UnauthenticatedClaimsProvider unauthenticatedClaimsProvider;
    
    @Autowired
    public OAuth2ClaimsProvider(
            AuthorizationBearerProvider authorizationBearerProvider,
            JsonObjectFactory jsonObjectFactory,
            UnauthenticatedClaimsProvider unauthenticatedClaimsProvider) {
        this.authorizationBearerProvider = authorizationBearerProvider;
        this.jsonObjectFactory = jsonObjectFactory;
        this.unauthenticatedClaimsProvider = unauthenticatedClaimsProvider;
    }

    @Override
    public Mono<JsonObject> getClaims(Request<JsonObjectWrapper> request) {
        return Mono.just(request)
                .flatMap(o -> Mono.justOrEmpty(authorizationBearerProvider.getBearer(request)))
                .map(this::parseClaims)
                .onErrorResume(error -> {
                    logger.error("Authentication failure", error);
                    return Mono.just(JsonObject.EMPTY);
                })
                .switchIfEmpty(unauthenticatedClaimsProvider.getUnauthenticatedClaims(request.getApiUriInfo().apiName()));
    }
    
    private JsonObject parseClaims(String token) {
        try {
            Map<String, Object> claims = JWTParser.parse(token)
                    .getJWTClaimsSet()
                    .getClaims();
            
            return jsonObjectFactory.createJsonObject(claims);
        } catch (ParseException e) {
            throw new JwtParseException(e);
        }
    }
}
