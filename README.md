# restbed-security-oauth2-resource-server

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-security-oauth2-resource-server.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-security-oauth2-resource-server)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-security-oauth2-resource-server/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-security-oauth2-resource-server)

This is the spring security oauth2 resource server implementation for Restbed modules. It implements the claim provider interface in the core project, providing support for spring security oauth2 resource authentication and authorization.

## Supported tags

* [`1.0.0.M5-59a5f14`, (*59a5f14/pom.xml*)](https://bitbucket.org/zuunr/restbed-security-oauth2-resource-server/src/59a5f14/pom.xml)

## Usage

To use this module, make sure this project's maven artifact is added as a dependency in your module, replacing 1.0.0-abcdefg with a supported tag:

```xml
<dependency>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-security-oauth2-resource-server</artifactId>
    <version>1.0.0-abcdefg</version>
</dependency>
```

## Configuration

The module requires additional configuration, see src/test/resources/application.yml for a sample.

Remember to use environment variables for security properties instead of the application.yml file in a non-test environment.
